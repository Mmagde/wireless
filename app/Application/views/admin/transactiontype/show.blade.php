@extends(layoutExtend())

@section('title')
    {{ trans('transactiontype.transactiontype') }} {{ trans('home.view') }}
@endsection

@section('content')
    @component(layoutForm() , ['title' => trans('transactiontype.transactiontype') , 'model' => 'transactiontype' , 'action' => trans('home.view')  ])
		 <table class="table table-bordered  table-striped" > 
				<tr>
				<th width="200">{{ trans("transactiontype.name") }}</th>
					<td>{{ nl2br($item->name) }}</td>
				</tr>
		</table>

        @include('admin.transactiontype.buttons.delete' , ['id' => $item->id])
        @include('admin.transactiontype.buttons.edit' , ['id' => $item->id])
    @endcomponent
@endsection
