@extends(layoutExtend())
  @section('title')
    {{ trans('transaction.transaction') }} {{ trans('home.view') }}
@endsection
  @section('content')
    @component(layoutForm() , ['title' => trans('transaction.transaction') , 'model' => 'transaction' , 'action' => trans('home.view')  ])
   <table class="table table-bordered  table-striped" > 
    <tr>
    <th width="200">{{ trans("transaction.notes") }}</th>
     <td>{{ nl2br($item->notes) }}</td>
    </tr>
    <tr>
    <th width="200">{{ trans("transaction.status") }}</th>
     <td>{{ nl2br($item->status) }}</td>
    </tr>
    <tr>
    <th width="200">{{ trans("transaction.amount") }}</th>
     <td>{{ nl2br($item->amount) }}</td>
    </tr>
  </table>
          @include('admin.transaction.buttons.delete' , ['id' => $item->id])
        @include('admin.transaction.buttons.edit' , ['id' => $item->id])
    @endcomponent
@endsection
