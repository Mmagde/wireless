		<div class="form-group {{ $errors->has("transactiontype") ? "has-error" : "" }}">
			<label for="transactiontype">{{ trans( "transactiontype.transactiontype") }}</label>
			@php $transactiontypes = App\Application\Model\Transactiontype::pluck("name" ,"id")->all()  @endphp
			@php  $transactiontype_id = isset($item) ? $item->transactiontype_id : null @endphp
			<select name="transactiontype_id"  class="form-control" >
			@foreach( $transactiontypes as $key => $relatedItem)
			<option value="{{ $key }}"  {{ $key == $transactiontype_id  ? "selected" : "" }}> {{ is_json($relatedItem) ? getDefaultValueKey($relatedItem) :  $relatedItem}}</option>
			@endforeach
			</select>
			@if ($errors->has("transactiontype"))
				<div class="alert alert-danger">
					<span class="help-block">
						<strong>{{ $errors->first("transactiontype") }}</strong>
					</span>
				</div>
			@endif
			</div>
