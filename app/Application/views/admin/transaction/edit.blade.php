@extends(layoutExtend())
 @section('title')
    {{ trans('transaction.transaction') }} {{  isset($item) ? trans('home.edit')  : trans('home.add')  }}
@endsection
 @section('content')
    @component(layoutForm() , ['title' => trans('transaction.transaction') , 'model' => 'transaction' , 'action' => isset($item) ? trans('home.edit')  : trans('home.add')  ])
         @include(layoutMessage())
        <form action="{{ concatenateLangToUrl('admin/transaction/item') }}{{ isset($item) ? '/'.$item->id : '' }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include("admin.transaction.relation.transactiontype.edit")
           <div class="form-group {{ $errors->has(" from_id ") ? "has-error " : " " }}">
                    <label for="account">From {{ trans( "account.account") }}</label> @php $accounts = App\Application\Model\Account::pluck("name"
                    ,"id")->all() 
@endphp @php $from_id = isset($item) ? $item->from_id : null 
@endphp
                    <select name="from_id" class="form-control">
                   @foreach( $accounts as $key => $relatedItem)
                   <option value="{{ $key }}"  {{ $key == $from_id  ? "selected" : "" }}> {{ is_json($relatedItem) ? getDefaultValueKey($relatedItem) :  $relatedItem}}</option>
                   @endforeach
                   </select> @if ($errors->has("from_id"))
                    <div class="alert alert-danger">
                        <span class="help-block">
                      <strong>{{ $errors->first("from_id") }}</strong>
                     </span>
                    </div>
                    @endif
                </div>
            <div class="form-group {{ $errors->has(" to_id ") ? "has-error " : " " }}">
            <label for="account"> To {{ trans( "account.account") }}</label> @php $accounts = App\Application\Model\Account::pluck("name"
            ,"id")->all() 
@endphp @php $to_id = isset($item) ? $item->to_id : null 
@endphp
            <select name="to_id" class="form-control">
           @foreach( $accounts as $key => $relatedItem)
           <option value="{{ $key }}"  {{ $key == $to_id  ? "selected" : "" }}> {{ is_json($relatedItem) ? getDefaultValueKey($relatedItem) :  $relatedItem}}</option>
           @endforeach
           </select> @if ($errors->has("to_id"))
            <div class="alert alert-danger">
                <span class="help-block">
              <strong>{{ $errors->first("to_id") }}</strong>
             </span>
            </div>
            @endif
        </div>
     <div class="form-group {{ $errors->has("notes") ? "has-error" : "" }}" > 
   <label for="notes">{{ trans("transaction.notes")}}</label>
    <input type="text" name="notes" class="form-control" id="notes" value="{{ isset($item->notes) ? $item->notes : old("notes") }}"  placeholder="{{ trans("transaction.notes")}}">
  </div>
   @if ($errors->has("notes"))
    <div class="alert alert-danger">
     <span class='help-block'>
      <strong>{{ $errors->first("notes") }}</strong>
     </span>
    </div>
   @endif
   <div class="form-group {{ $errors->has("status") ? "has-error" : "" }}" > 
   <label for="status">{{ trans("transaction.status")}}</label>
    <input type="text" name="status" class="form-control" id="status" value="{{ isset($item->status) ? $item->status : old("status") }}"  placeholder="{{ trans("transaction.status")}}">
  </div>
   @if ($errors->has("status"))
    <div class="alert alert-danger">
     <span class='help-block'>
      <strong>{{ $errors->first("status") }}</strong>
     </span>
    </div>
   @endif
   <div class="form-group {{ $errors->has("amount") ? "has-error" : "" }}" > 
   <label for="amount">{{ trans("transaction.amount")}}</label>
    <input type="text" name="amount" class="form-control" id="amount" value="{{ isset($item->amount) ? $item->amount : old("amount") }}"  placeholder="{{ trans("transaction.amount")}}">
  </div>
   @if ($errors->has("amount"))
    <div class="alert alert-danger">
     <span class='help-block'>
      <strong>{{ $errors->first("amount") }}</strong>
     </span>
    </div>
   @endif
              <div class="form-group">
                <button type="submit" name="submit" class="btn btn-default" >
                    <i class="material-icons">check_circle</i>
                    {{ trans('home.save') }}  {{ trans('transaction.transaction') }}
                </button>
            </div>
        </form>
    @endcomponent
@endsection
