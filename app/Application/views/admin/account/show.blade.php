@extends(layoutExtend())
  @section('title')
    {{ trans('account.account') }} {{ trans('home.view') }}
@endsection
  @section('content')
    @component(layoutForm() , ['title' => trans('account.account') , 'model' => 'account' , 'action' => trans('home.view')  ])
   <table class="table table-bordered  table-striped" > 
    <tr>
    <th width="200">{{ trans("account.name") }}</th>
     <td>{{ nl2br($item->name) }}</td>
    </tr>
    <tr>
    <th width="200">{{ trans("account.balance") }}</th>
     <td>{{ nl2br($item->balance) }}</td>
    </tr>
  </table>
          @include('admin.account.buttons.delete' , ['id' => $item->id])
        @include('admin.account.buttons.edit' , ['id' => $item->id])
    @endcomponent
@endsection
