<h2>{{ ucfirst(trans('admin.Latest'))}} {{ ucfirst('transactiontype') }}</h2>
<hr>
@php $sidebarTransactiontype = \App\Application\Model\Transactiontype::orderBy("id", "DESC")->limit(5)->get(); @endphp
		@if (count($sidebarTransactiontype) > 0)
			@foreach ($sidebarTransactiontype as $d)
				 <div>
					<p><a href="{{ url("transactiontype/".$d->id."/view") }}">{{ str_limit($d->name , 20) }}</a></p > 
					<p><a href="{{ url("transactiontype/".$d->id."/view") }}" ><i class="fa fa-eye" ></i ></a> <small ><i class="fa fa-calendar-o" ></i > {{ $d->created_at }}</small ></p > 
				<hr > 
				</div> 
			@endforeach
		@endif
			