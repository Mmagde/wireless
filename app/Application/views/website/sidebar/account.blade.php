<h2>{{ ucfirst(trans('admin.Latest'))}} {{ ucfirst('account') }}</h2>
<hr>
@php $sidebarAccount = \App\Application\Model\Account::orderBy("id", "DESC")->limit(5)->get(); @endphp
		@if (count($sidebarAccount) > 0)
			@foreach ($sidebarAccount as $d)
				 <div>
					<p><a href="{{ url("account/".$d->id."/view") }}">{{ str_limit($d->name , 20) }}</a></p > 
					<p><a href="{{ url("account/".$d->id."/view") }}" ><i class="fa fa-eye" ></i ></a> <small ><i class="fa fa-calendar-o" ></i > {{ $d->created_at }}</small ></p > 
				<hr > 
				</div> 
			@endforeach
		@endif
			