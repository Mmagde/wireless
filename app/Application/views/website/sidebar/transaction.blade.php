<h2>{{ ucfirst(trans('admin.Latest'))}} {{ ucfirst('transaction') }}</h2>
<hr>
@php $sidebarTransaction = \App\Application\Model\Transaction::orderBy("id", "DESC")->limit(5)->get(); @endphp
		@if (count($sidebarTransaction) > 0)
			@foreach ($sidebarTransaction as $d)
				 <div>
					<p><a href="{{ url("transaction/".$d->id."/view") }}">{{ str_limit($d->notes , 20) }}</a></p > 
					<p><a href="{{ url("transaction/".$d->id."/view") }}" ><i class="fa fa-eye" ></i ></a> <small ><i class="fa fa-calendar-o" ></i > {{ $d->created_at }}</small ></p > 
				<hr > 
				</div> 
			@endforeach
		@endif
			