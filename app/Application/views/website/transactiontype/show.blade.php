@extends(layoutExtend('website'))

@section('title')
    {{ trans('transactiontype.transactiontype') }} {{ trans('home.view') }}
@endsection

@section('content')
<div class="pull-{{ getDirection() }} col-lg-9">
        <a href="{{ url('transactiontype') }}" class="btn btn-danger"><i class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
		 <table class="table table-bordered  table-striped" > 
				<tr>
				<th width="200">{{ trans("transactiontype.name") }}</th>
					<td>{{ nl2br($item->name) }}</td>
				</tr>
		</table>

        @include('website.transactiontype.buttons.delete' , ['id' => $item->id])
        @include('website.transactiontype.buttons.edit' , ['id' => $item->id])
</div>
@endsection
