<h2>{{ ucfirst(trans('admin.Random'))}} {{ ucfirst('account') }}</h2>
<hr>
@php $sidebarAccount = \App\Application\Model\Account::inRandomOrder()->limit(5)->get(); @endphp
		@if (count($sidebarAccount) > 0)
			@foreach ($sidebarAccount as $d)
				 <div>
					<h2 > {{ str_limit($d->name , 50) }}</h2 > 
					<p> {{ str_limit($d->balance , 300) }}</p > 
					 <p><a href="{{ url("account/".$d->id."/view") }}" ><i class="fa fa-eye" ></i ></a> <small ><i class="fa fa-calendar-o" ></i > {{ $d->created_at }}</small ></p > 
				<hr > 
				</div> 
			@endforeach
		@endif
			