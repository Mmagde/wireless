<h2>{{ ucfirst(trans('admin.Random'))}} {{ ucfirst('transactiontype') }}</h2>
<hr>
@php $sidebarTransactiontype = \App\Application\Model\Transactiontype::inRandomOrder()->limit(5)->get(); @endphp
		@if (count($sidebarTransactiontype) > 0)
			@foreach ($sidebarTransactiontype as $d)
				 <div>
					<h2 > {{ str_limit($d->name , 50) }}</h2 > 
					 <p><a href="{{ url("transactiontype/".$d->id."/view") }}" ><i class="fa fa-eye" ></i ></a> <small ><i class="fa fa-calendar-o" ></i > {{ $d->created_at }}</small ></p > 
				<hr > 
				</div> 
			@endforeach
		@endif
			