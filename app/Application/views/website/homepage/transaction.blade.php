<h2>{{ ucfirst(trans('admin.Random'))}} {{ ucfirst('transaction') }}</h2>
<hr>
@php $sidebarTransaction = \App\Application\Model\Transaction::inRandomOrder()->limit(5)->get(); @endphp
		@if (count($sidebarTransaction) > 0)
			@foreach ($sidebarTransaction as $d)
				 <div>
					<h2 > {{ str_limit($d->notes , 50) }}</h2 > 
					<p> {{ str_limit($d->status , 300) }}</p > 
					<p> {{ str_limit($d->amount , 300) }}</p > 
					 <p><a href="{{ url("transaction/".$d->id."/view") }}" ><i class="fa fa-eye" ></i ></a> <small ><i class="fa fa-calendar-o" ></i > {{ $d->created_at }}</small ></p > 
				<hr > 
				</div> 
			@endforeach
		@endif
			