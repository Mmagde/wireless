@extends(layoutExtend('website'))
 @section('title')
    {{ trans('account.account') }} {{  isset($item) ? trans('home.edit')  : trans('home.add')  }}
@endsection
 @section('content')
<div class="pull-{{ getDirection() }} col-lg-9">
         @include(layoutMessage('website'))
         <a href="{{ url('account') }}" class="btn btn-danger"><i class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
        <form action="{{ concatenateLangToUrl('account/item') }}{{ isset($item) ? '/'.$item->id : '' }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include("website.account.relation.user.edit")
                <div class="form-group {{ $errors->has("name") ? "has-error" : "" }}" > 
   <label for="name">{{ trans("account.name")}}</label>
    <input type="text" name="name" class="form-control" id="name" value="{{ isset($item->name) ? $item->name : old("name") }}"  placeholder="{{ trans("account.name")}}">
  </div>
   @if ($errors->has("name"))
    <div class="alert alert-danger">
     <span class='help-block'>
      <strong>{{ $errors->first("name") }}</strong>
     </span>
    </div>
   @endif
   <div class="form-group {{ $errors->has("balance") ? "has-error" : "" }}" > 
   <label for="balance">{{ trans("account.balance")}}</label>
    <input type="text" name="balance" class="form-control" id="balance" value="{{ isset($item->balance) ? $item->balance : old("balance") }}"  placeholder="{{ trans("account.balance")}}">
  </div>
   @if ($errors->has("balance"))
    <div class="alert alert-danger">
     <span class='help-block'>
      <strong>{{ $errors->first("balance") }}</strong>
     </span>
    </div>
   @endif
             <div class="form-group">
                <button type="submit" name="submit" class="btn btn-default" >
                    <i class="fa fa-save"></i>
                    {{ trans('website.Update') }}  {{ trans('website.account') }}
                </button>
            </div>
        </form>
</div>
@endsection
