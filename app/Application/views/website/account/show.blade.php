@extends(layoutExtend('website'))
  @section('title')
    {{ trans('account.account') }} {{ trans('home.view') }}
@endsection
  @section('content')
<div class="pull-{{ getDirection() }} col-lg-9">
        <a href="{{ url('account') }}" class="btn btn-danger"><i class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
   <table class="table table-bordered  table-striped" > 
    <tr>
    <th width="200">{{ trans("account.name") }}</th>
     <td>{{ nl2br($item->name) }}</td>
    </tr>
    <tr>
    <th width="200">{{ trans("account.balance") }}</th>
     <td>{{ nl2br($item->balance) }}</td>
    </tr>
  </table>
          @include('website.account.buttons.delete' , ['id' => $item->id])
        @include('website.account.buttons.edit' , ['id' => $item->id])
</div>
@endsection
