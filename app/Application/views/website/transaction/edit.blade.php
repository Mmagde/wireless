@extends(layoutExtend('website'))
 @section('title')
    {{ trans('transaction.transaction') }} {{  isset($item) ? trans('home.edit')  : trans('home.add')  }}
@endsection
 @section('content')
<div class="pull-{{ getDirection() }} col-lg-9">
         @include(layoutMessage('website'))
         <a href="{{ url('transaction') }}" class="btn btn-danger"><i class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
        <form action="{{ concatenateLangToUrl('transaction/item') }}{{ isset($item) ? '/'.$item->id : '' }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include("website.transaction.relation.transactiontype.edit")
            @include("website.transaction.relation.account.edit")
            @include("website.transaction.relation.account.edit")
                <div class="form-group {{ $errors->has("notes") ? "has-error" : "" }}" > 
   <label for="notes">{{ trans("transaction.notes")}}</label>
    <input type="text" name="notes" class="form-control" id="notes" value="{{ isset($item->notes) ? $item->notes : old("notes") }}"  placeholder="{{ trans("transaction.notes")}}">
  </div>
   @if ($errors->has("notes"))
    <div class="alert alert-danger">
     <span class='help-block'>
      <strong>{{ $errors->first("notes") }}</strong>
     </span>
    </div>
   @endif
   <div class="form-group {{ $errors->has("status") ? "has-error" : "" }}" > 
   <label for="status">{{ trans("transaction.status")}}</label>
    <input type="text" name="status" class="form-control" id="status" value="{{ isset($item->status) ? $item->status : old("status") }}"  placeholder="{{ trans("transaction.status")}}">
  </div>
   @if ($errors->has("status"))
    <div class="alert alert-danger">
     <span class='help-block'>
      <strong>{{ $errors->first("status") }}</strong>
     </span>
    </div>
   @endif
   <div class="form-group {{ $errors->has("amount") ? "has-error" : "" }}" > 
   <label for="amount">{{ trans("transaction.amount")}}</label>
    <input type="text" name="amount" class="form-control" id="amount" value="{{ isset($item->amount) ? $item->amount : old("amount") }}"  placeholder="{{ trans("transaction.amount")}}">
  </div>
   @if ($errors->has("amount"))
    <div class="alert alert-danger">
     <span class='help-block'>
      <strong>{{ $errors->first("amount") }}</strong>
     </span>
    </div>
   @endif
             <div class="form-group">
                <button type="submit" name="submit" class="btn btn-default" >
                    <i class="fa fa-save"></i>
                    {{ trans('website.Update') }}  {{ trans('website.transaction') }}
                </button>
            </div>
        </form>
</div>
@endsection
