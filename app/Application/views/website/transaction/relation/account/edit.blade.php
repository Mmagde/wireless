		<div class="form-group {{ $errors->has("account") ? "has-error" : "" }}">
			<label for="account">{{ trans( "account.account") }}</label>
			@php $accounts = App\Application\Model\Account::pluck("name" ,"id")->all()  @endphp
			@php  $account_id = isset($item) ? $item->account_id : null @endphp
			<select name="account_id"  class="form-control" >
			@foreach( $accounts as $key => $relatedItem)
			<option value="{{ $key }}"  {{ $key == $account_id  ? "selected" : "" }}> {{ is_json($relatedItem) ? getDefaultValueKey($relatedItem) :  $relatedItem}}</option>
			@endforeach
			</select>
			@if ($errors->has("account"))
				<div class="alert alert-danger">
					<span class="help-block">
						<strong>{{ $errors->first("account") }}</strong>
					</span>
				</div>
			@endif
			</div>
