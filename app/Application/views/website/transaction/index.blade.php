@extends(layoutExtend('website'))

@section('title')
     {{ trans('transaction.transaction') }} {{ trans('home.control') }}
@endsection

@section('content')
 <div class="pull-{{ getDirection() }} col-lg-9">
    <div><h1>{{ trans('website.transaction') }}</h1></div>
     <div><a href="{{ url('transaction/item') }}" class="btn btn-default"><i class="fa fa-plus"></i> {{ trans('website.transaction') }}</a><br></div>
 	<form method="get" class="form-inline">
		<div class="form-group">
			<input type="text" name="from" class="form-control datepicker2" placeholder="{{ trans("admin.from") }}"value="{{ request()->has("from") ? request()->get("from") : "" }}">
		 </div>
		<div class="form-group">
			<input type="text" name="to" class="form-control datepicker2" placeholder="{{ trans("admin.to") }}"value="{{ request()->has("to") ? request()->get("to") : "" }}">
		</div>
		<div class="form-group"> 
			<input type="text" name="notes" class="form-control " placeholder="{{ trans("transaction.notes") }}" value="{{ request()->has("notes") ? request()->get("notes") : "" }}"> 
		</div> 
		<div class="form-group"> 
			<input type="text" name="status" class="form-control " placeholder="{{ trans("transaction.status") }}" value="{{ request()->has("status") ? request()->get("status") : "" }}"> 
		</div> 
		<div class="form-group"> 
			<input type="text" name="amount" class="form-control " placeholder="{{ trans("transaction.amount") }}" value="{{ request()->has("amount") ? request()->get("amount") : "" }}"> 
		</div> 
		 <button class="btn btn-success" type="submit" ><i class="fa fa-search" ></i ></button>
		<a href="{{ url("transaction") }}" class="btn btn-danger" ><i class="fa fa-close" ></i></a>
	 </form > 
<br ><table class="table table-responsive table-striped table-bordered"> 
		<thead > 
			<tr> 
				<th>{{ trans("transaction.notes") }}</th> 
				<th>{{ trans("transaction.edit") }}</th> 
				<th>{{ trans("transaction.show") }}</th> 
				<th>{{
            trans("transaction.delete") }}</th> 
				</thead > 
		<tbody > 
		@if (count($items) > 0) 
			@foreach ($items as $d) 
				 <tr>
					<td>{{ str_limit($d->notes , 20) }}</td> 
				<td> @include("website.transaction.buttons.edit", ["id" => $d->id])</td> 
					<td> @include("website.transaction.buttons.view", ["id" => $d->id])</td> 
					<td> @include("website.transaction.buttons.delete", ["id" => $d->id])</td> 
					</tr> 
					@endforeach
				@endif
			 </tbody > 
		</table > 
	@include(layoutPaginate() , ["items" => $items])
		
</div>
@endsection
