@extends(layoutExtend('website'))
  @section('title')
    {{ trans('transaction.transaction') }} {{ trans('home.view') }}
@endsection
  @section('content')
<div class="pull-{{ getDirection() }} col-lg-9">
        <a href="{{ url('transaction') }}" class="btn btn-danger"><i class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
   <table class="table table-bordered  table-striped" > 
    <tr>
    <th width="200">{{ trans("transaction.notes") }}</th>
     <td>{{ nl2br($item->notes) }}</td>
    </tr>
    <tr>
    <th width="200">{{ trans("transaction.status") }}</th>
     <td>{{ nl2br($item->status) }}</td>
    </tr>
    <tr>
    <th width="200">{{ trans("transaction.amount") }}</th>
     <td>{{ nl2br($item->amount) }}</td>
    </tr>
  </table>
          @include('website.transaction.buttons.delete' , ['id' => $item->id])
        @include('website.transaction.buttons.edit' , ['id' => $item->id])
</div>
@endsection
