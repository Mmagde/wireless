<?php
 namespace App\Application\Model;
 use Illuminate\Database\Eloquent\Model;
 class Transaction extends Model
{
   public $table = "transaction";
   public function transactiontype(){
		return $this->belongsTo(Transactiontype::class, "transactiontype_id");
		}
    public function from(){
  return $this->belongsTo(Account::class, "from_id");
  }
   public function to(){
  return $this->belongsTo(Account::class, "to_id");
  }
     protected $fillable = [
     'transactiontype_id',
     'account_id',
     'account_id',
     'from_id',
   'to_id',
        'notes','status','amount'
   ];
  }
