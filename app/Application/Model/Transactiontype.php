<?php
 namespace App\Application\Model;
 use Illuminate\Database\Eloquent\Model;
 class Transactiontype extends Model
{
   public $table = "transactiontype";
  public function transaction(){
		return $this->hasMany(Transaction::class, "transactiontype_id");
		}
     protected $fillable = [
        'name'
   ];
  }
