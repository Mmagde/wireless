<?php
 namespace App\Application\Model;
 use Illuminate\Database\Eloquent\Model;
 class Account extends Model
{
   public $table = "account";
 
   public function user(){
  return $this->belongsTo(User::class, "user_id");
  }
     protected $fillable = [
   'user_id',
        'name','balance'
   ];
  }
