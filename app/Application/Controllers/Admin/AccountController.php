<?php

namespace App\Application\Controllers\Admin;

use App\Application\Requests\Admin\Account\AddRequestAccount;
use App\Application\Requests\Admin\Account\UpdateRequestAccount;
use App\Application\Controllers\AbstractController;
use App\Application\DataTables\AccountsDataTable;
use App\Application\Model\Account;
use Yajra\Datatables\Request;
use Alert;

class AccountController extends AbstractController
{
    public function __construct(Account $model)
    {
        parent::__construct($model);
    }

    public function index(AccountsDataTable $dataTable){
        return $dataTable->render('admin.account.index');
    }

    public function show($id = null){
        return $this->createOrEdit('admin.account.edit' , $id);
    }

     public function store(AddRequestAccount $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('admin/account');
     }

     public function update($id , UpdateRequestAccount $request){
          $item = $this->storeOrUpdate($request, $id, true);
return redirect()->back();

     }


    public function getById($id){
        $fields = $this->model->findOrFail($id);
        return $this->createOrEdit('admin.account.show' , $id , ['fields' =>  $fields]);
    }

    public function destroy($id){
        return $this->deleteItem($id , 'admin/account')->with('sucess' , 'Done Delete account From system');
    }

    public function pluck(\Illuminate\Http\Request $request){
        return $this->deleteItem($request->id , 'admin/account')->with('sucess' , 'Done Delete account From system');
    }

}
