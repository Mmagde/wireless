<?php

namespace App\Application\Controllers\Admin;

use App\Application\Requests\Admin\Transactiontype\AddRequestTransactiontype;
use App\Application\Requests\Admin\Transactiontype\UpdateRequestTransactiontype;
use App\Application\Controllers\AbstractController;
use App\Application\DataTables\TransactiontypesDataTable;
use App\Application\Model\Transactiontype;
use Yajra\Datatables\Request;
use Alert;

class TransactiontypeController extends AbstractController
{
    public function __construct(Transactiontype $model)
    {
        parent::__construct($model);
    }

    public function index(TransactiontypesDataTable $dataTable){
        return $dataTable->render('admin.transactiontype.index');
    }

    public function show($id = null){
        return $this->createOrEdit('admin.transactiontype.edit' , $id);
    }

     public function store(AddRequestTransactiontype $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('admin/transactiontype');
     }

     public function update($id , UpdateRequestTransactiontype $request){
          $item = $this->storeOrUpdate($request, $id, true);
return redirect()->back();

     }


    public function getById($id){
        $fields = $this->model->findOrFail($id);
        return $this->createOrEdit('admin.transactiontype.show' , $id , ['fields' =>  $fields]);
    }

    public function destroy($id){
        return $this->deleteItem($id , 'admin/transactiontype')->with('sucess' , 'Done Delete transactiontype From system');
    }

    public function pluck(\Illuminate\Http\Request $request){
        return $this->deleteItem($request->id , 'admin/transactiontype')->with('sucess' , 'Done Delete transactiontype From system');
    }

}
