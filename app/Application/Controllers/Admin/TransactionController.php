<?php

namespace App\Application\Controllers\Admin;

use App\Application\Requests\Admin\Transaction\AddRequestTransaction;
use App\Application\Requests\Admin\Transaction\UpdateRequestTransaction;
use App\Application\Controllers\AbstractController;
use App\Application\DataTables\TransactionsDataTable;
use App\Application\Model\Transaction;
use Yajra\Datatables\Request;
use Alert;
use App\Application\Model\Account;

class TransactionController extends AbstractController
{
    public function __construct(Transaction $model)
    {
        parent::__construct($model);
    }

    public function index(TransactionsDataTable $dataTable){
        return $dataTable->render('admin.transaction.index');
    }

    public function show($id = null){
        return $this->createOrEdit('admin.transaction.edit' , $id);
    }

     public function store(AddRequestTransaction $request){
         if($request->transactiontype_id == 2){
            $from = Account::find($request->from_id);
            $to = Account::find($request->to_id);
           try{
            if($request->amount <= $from->balance){
                $from->balance = $from->balance - $request->amount;
                $to->balance = $to->balance + $request->amount;
                $from->save(); 
                $to->save();
            }
            else {
               return redirect('404');
            }
          }catch(\Exception $e){
                return redirect('404');
          }
         
         }
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('admin/transaction');
     }

     public function update($id , UpdateRequestTransaction $request){
          $item = $this->storeOrUpdate($request, $id, true);
return redirect()->back();

     }


    public function getById($id){
        $fields = $this->model->findOrFail($id);
        return $this->createOrEdit('admin.transaction.show' , $id , ['fields' =>  $fields]);
    }

    public function destroy($id){
        return $this->deleteItem($id , 'admin/transaction')->with('sucess' , 'Done Delete transaction From system');
    }

    public function pluck(\Illuminate\Http\Request $request){
        return $this->deleteItem($request->id , 'admin/transaction')->with('sucess' , 'Done Delete transaction From system');
    }

}
