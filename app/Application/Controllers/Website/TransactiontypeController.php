<?php

namespace App\Application\Controllers\Website;

use App\Application\Controllers\AbstractController;
use Alert;
use App\Application\Model\Transactiontype;
use App\Application\Requests\Website\Transactiontype\AddRequestTransactiontype;
use App\Application\Requests\Website\Transactiontype\UpdateRequestTransactiontype;

class TransactiontypeController extends AbstractController
{

     public function __construct(Transactiontype $model)
     {
        parent::__construct($model);
     }

     public function index(){
        $items = $this->model;

        if(request()->has('from') && request()->get('from') != ''){
            $items = $items->whereDate('created_at' , '>=' , request()->get('from'));
        }

        if(request()->has('to') && request()->get('to') != ''){
            $items = $items->whereDate('created_at' , '<=' , request()->get('to'));
        }

			if(request()->has("name") && request()->get("name") != ""){
				$items = $items->where("name","=", request()->get("name"));
			}



        $items = $items->paginate(env('PAGINATE'));
        return view('website.transactiontype.index' , compact('items'));
     }

     public function show($id = null){
         return $this->createOrEdit('website.transactiontype.edit' , $id);
     }

     public function store(AddRequestTransactiontype $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('transactiontype');
     }

     public function update($id , UpdateRequestTransactiontype $request){
          $item = $this->storeOrUpdate($request, $id, true);
return redirect()->back();

     }

     public function getById($id){
         $fields = $this->model->findOrFail($id);
         return $this->createOrEdit('website.transactiontype.show' , $id , ['fields' =>  $fields]);
     }

     public function destroy($id){
         return $this->deleteItem($id , 'transactiontype')->with('sucess' , 'Done Delete Transactiontype From system');
     }


}
