<?php

namespace App\Application\Controllers\Website;

use App\Application\Controllers\AbstractController;
use Alert;
use App\Application\Model\Account;
use App\Application\Requests\Website\Account\AddRequestAccount;
use App\Application\Requests\Website\Account\UpdateRequestAccount;

class AccountController extends AbstractController
{

     public function __construct(Account $model)
     {
        parent::__construct($model);
     }

     public function index(){
        $items = $this->model;

        if(request()->has('from') && request()->get('from') != ''){
            $items = $items->whereDate('created_at' , '>=' , request()->get('from'));
        }

        if(request()->has('to') && request()->get('to') != ''){
            $items = $items->whereDate('created_at' , '<=' , request()->get('to'));
        }

			if(request()->has("name") && request()->get("name") != ""){
				$items = $items->where("name","=", request()->get("name"));
			}

			if(request()->has("balance") && request()->get("balance") != ""){
				$items = $items->where("balance","=", request()->get("balance"));
			}



        $items = $items->paginate(env('PAGINATE'));
        return view('website.account.index' , compact('items'));
     }

     public function show($id = null){
         return $this->createOrEdit('website.account.edit' , $id);
     }

     public function store(AddRequestAccount $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('account');
     }

     public function update($id , UpdateRequestAccount $request){
          $item = $this->storeOrUpdate($request, $id, true);
return redirect()->back();

     }

     public function getById($id){
         $fields = $this->model->findOrFail($id);
         return $this->createOrEdit('website.account.show' , $id , ['fields' =>  $fields]);
     }

     public function destroy($id){
         return $this->deleteItem($id , 'account')->with('sucess' , 'Done Delete Account From system');
     }


}
