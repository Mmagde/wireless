<?php

namespace App\Application\Controllers\Website;

use App\Application\Controllers\AbstractController;
use Alert;
use App\Application\Model\Transaction;
use App\Application\Requests\Website\Transaction\AddRequestTransaction;
use App\Application\Requests\Website\Transaction\UpdateRequestTransaction;

class TransactionController extends AbstractController
{

     public function __construct(Transaction $model)
     {
        parent::__construct($model);
     }

     public function index(){
        $items = $this->model;

        if(request()->has('from') && request()->get('from') != ''){
            $items = $items->whereDate('created_at' , '>=' , request()->get('from'));
        }

        if(request()->has('to') && request()->get('to') != ''){
            $items = $items->whereDate('created_at' , '<=' , request()->get('to'));
        }

			if(request()->has("notes") && request()->get("notes") != ""){
				$items = $items->where("notes","=", request()->get("notes"));
			}

			if(request()->has("status") && request()->get("status") != ""){
				$items = $items->where("status","=", request()->get("status"));
			}

			if(request()->has("amount") && request()->get("amount") != ""){
				$items = $items->where("amount","=", request()->get("amount"));
			}



        $items = $items->paginate(env('PAGINATE'));
        return view('website.transaction.index' , compact('items'));
     }

     public function show($id = null){
         return $this->createOrEdit('website.transaction.edit' , $id);
     }

     public function store(AddRequestTransaction $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('transaction');
     }

     public function update($id , UpdateRequestTransaction $request){
          $item = $this->storeOrUpdate($request, $id, true);
return redirect()->back();

     }

     public function getById($id){
         $fields = $this->model->findOrFail($id);
         return $this->createOrEdit('website.transaction.show' , $id , ['fields' =>  $fields]);
     }

     public function destroy($id){
         return $this->deleteItem($id , 'transaction')->with('sucess' , 'Done Delete Transaction From system');
     }


}
