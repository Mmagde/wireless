<?php

namespace App\Application\Controllers\Api;


use App\Application\Controllers\Controller;
use App\Application\Model\Account;
use App\Application\Transformers\AccountTransformers;
use App\Application\Requests\Website\Account\ApiAddRequestAccount;
use App\Application\Requests\Website\Account\ApiUpdateRequestAccount;

class AccountApi extends Controller
{
    use ApiTrait;
    protected $model;

    public function __construct(Account $model)
    {
        $this->model = $model;
        /// send header Authorization Bearer token
         $this->middleware('authApi')->only('index');
    }
    public function index()
    {
        $user = auth()->guard('api')->user();
        if($user->account){
            return response(apiReturn(AccountTransformers::transform($user->account)), 200);
        }
        return response(apiReturn('', 'error', 'Something went wrong'), 401);
    }
    public function add(ApiAddRequestAccount $validation){
         return $this->addItem($validation);
    }

    public function update($id , ApiUpdateRequestAccount $validation){
        return $this->updateItem($id , $validation);
    }

    protected function checkLanguageBeforeReturn($data , $status_code = 200, $paginate = [])
    {
       if (request()->has('lang') && request()->get('lang') == 'ar') {
            return response(apiReturn(AccountTransformers::transformAr($data) + $paginate), $status_code);
        }
        return response(apiReturn(AccountTransformers::transform($data) + $paginate), $status_code);
    }

}
