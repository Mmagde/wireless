<?php

namespace App\Application\Controllers\Api;


use App\Application\Controllers\Controller;
use App\Application\Model\Transaction;
use App\Application\Model\Account;
use App\Application\Transformers\TransactionTransformers;
use App\Application\Requests\Website\Transaction\ApiAddRequestTransaction;
use App\Application\Requests\Website\Transaction\ApiUpdateRequestTransaction;

class TransactionApi extends Controller
{
    use ApiTrait;
    protected $model;

    public function __construct(Transaction $model)
    {
        $this->model = $model;
        /// send header Authorization Bearer token
         $this->middleware('authApi')->only('add' ,'index', 'getByAcc');
    }
    public function index()
    {
        $user = auth()->guard('api')->user();
        $accounts = Account::where('user_id' , $user->id)->pluck('id');
        $transactions = Transaction::whereIn('from_id' , $accounts)->orWhereIn('to_id', $accounts)->get();
        if($transactions)
            return response(apiReturn(TransactionTransformers::transform($transactions)), 200);
        
        return response(apiReturn('', 'error', 'Something went wrong'), 401);
    }
    public function add(ApiAddRequestTransaction $validation){
        //dd($validation);
        $user = auth()->guard('api')->user();
        $from = Account::find(request()->from_id);
        if($from->user->id != $user->id){
            return response(apiReturn('', 'error', 'this account is not yours!'), 404);
        }
        if (request()->transactiontype_id == 2) {
            $to = Account::find(request()->to_id);
            try {
                if (request()->amount <= $from->balance) {
                    $from->balance = $from->balance - request()->amount;
                    $to->balance = $to->balance + request()->amount;
                    $from->save();
                    $to->save();
                } else {
                    return response(apiReturn('', 'error', 'not enough balance!'), 404);
                }
            } catch (\Exception $e) {
                return response(apiReturn('', 'error', 'something went wrong!'), 404);
            }

        }
         return $this->addItem($validation);
    }

    public function update($id , ApiUpdateRequestTransaction $validation){
        return $this->updateItem($id , $validation);
    }
    public function getByAcc($id)
    {
        $user = auth()->guard('api')->user();
        $accounts = Account::where('user_id', $user->id)->pluck('id');
        if(in_array($id , $accounts->all())){
            $transactions = Transaction::where('from_id', $id)->orWhere('to_id', $id)->get();
            if ($transactions)
                return response(apiReturn(TransactionTransformers::transform($transactions)), 200);
        }
        return response(apiReturn('', 'error', 'Something went wrong'), 401);
    }
    protected function checkLanguageBeforeReturn($data , $status_code = 200, $paginate = [])
    {
       if (request()->has('lang') && request()->get('lang') == 'ar') {
            return response(apiReturn(TransactionTransformers::transformAr($data) + $paginate), $status_code);
        }
        return response(apiReturn(TransactionTransformers::transform($data) + $paginate), $status_code);
    }

}
