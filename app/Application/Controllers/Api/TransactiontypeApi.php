<?php

namespace App\Application\Controllers\Api;


use App\Application\Controllers\Controller;
use App\Application\Model\Transactiontype;
use App\Application\Transformers\TransactiontypeTransformers;
use App\Application\Requests\Website\Transactiontype\ApiAddRequestTransactiontype;
use App\Application\Requests\Website\Transactiontype\ApiUpdateRequestTransactiontype;

class TransactiontypeApi extends Controller
{
    use ApiTrait;
    protected $model;

    public function __construct(Transactiontype $model)
    {
        $this->model = $model;
        /// send header Authorization Bearer token
        /// $this->middleware('authApi')->only();
    }

    public function add(ApiAddRequestTransactiontype $validation){
         return $this->addItem($validation);
    }

    public function update($id , ApiUpdateRequestTransactiontype $validation){
        return $this->updateItem($id , $validation);
    }

    protected function checkLanguageBeforeReturn($data , $status_code = 200, $paginate = [])
    {
       if (request()->has('lang') && request()->get('lang') == 'ar') {
            return response(apiReturn(TransactiontypeTransformers::transformAr($data) + $paginate), $status_code);
        }
        return response(apiReturn(TransactiontypeTransformers::transform($data) + $paginate), $status_code);
    }

}
