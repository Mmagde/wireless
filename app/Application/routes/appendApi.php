<?php

#user
Route::post('users/login', 'UserApi@login');
Route::get('users/getById/{id}', 'UserApi@getById');
Route::get('users/delete/{id}', 'UserApi@delete');
Route::post('users/add', 'UserApi@add');
Route::post('users/update', 'UserApi@update');
Route::get('users', 'UserApi@index');
Route::get('users/getUserByToken', 'UserApi@getUserByToken');

#page
Route::get('page/getById/{id}', 'PageApi@getById');
Route::get('page/delete/{id}', 'PageApi@delete');
Route::post('page/add', 'PageApi@add');
Route::post('page/update/{id}', 'PageApi@update');
Route::get('page', 'PageApi@index');

#categorie
Route::get('categorie/getById/{id}', 'CategorieApi@getById');
Route::get('categorie/delete/{id}', 'CategorieApi@delete');
Route::post('categorie/add', 'CategorieApi@add');
Route::post('categorie/update/{id}', 'CategorieApi@update');
Route::get('categorie', 'CategorieApi@index');









#account
#Route::get('account/getById/{id}', 'AccountApi@getById');
#Route::get('account/delete/{id}', 'AccountApi@delete');
#Route::post('account/add', 'AccountApi@add');
#Route::post('account/update/{id}', 'AccountApi@update');
Route::get('account', 'AccountApi@index');

#transactiontype
Route::get('transactiontype/getById/{id}', 'TransactiontypeApi@getById');
Route::get('transactiontype/delete/{id}', 'TransactiontypeApi@delete');
Route::post('transactiontype/add', 'TransactiontypeApi@add');
Route::post('transactiontype/update/{id}', 'TransactiontypeApi@update');
Route::get('transactiontype', 'TransactiontypeApi@index');

#transaction
Route::get('transaction/getById/{id}', 'TransactionApi@getById');
Route::get('transaction/getByAcc/{id}', 'TransactionApi@getByAcc');
Route::get('transaction/delete/{id}', 'TransactionApi@delete');
Route::post('transaction/add', 'TransactionApi@add');
Route::post('transaction/update/{id}', 'TransactionApi@update');
Route::get('transaction', 'TransactionApi@index');