<?php
 namespace App\Application\Requests\Website\Account;
 use Illuminate\Support\Facades\Route;
 class ApiUpdateRequestAccount
{
    public function rules()
    {
        $id = Route::input('id');
        return [
        	"user_id" => "required|integer",
            "name" => "required",
   "balance" => "required",
            ];
    }
}
