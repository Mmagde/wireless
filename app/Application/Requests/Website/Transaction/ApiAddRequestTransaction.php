<?php
 namespace App\Application\Requests\Website\Transaction;
  class ApiAddRequestTransaction
{
    public function rules()
    {
        return [
        	"transactiontype_id" => "required|integer",
            "from_id" => "required|integer",
            "to_id" => "nullable|integer",
            "notes" => "",
            "status" => "",
            "amount" => "required|integer",
            ];
    }
}
