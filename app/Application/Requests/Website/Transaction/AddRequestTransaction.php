<?php
 namespace App\Application\Requests\Website\Transaction;
 use Illuminate\Foundation\Http\FormRequest;
 class AddRequestTransaction extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
     /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        	"transactiontype_id" => "required|integer",
         "account_id" => "required|integer",
         "account_id" => "required|integer",
            "notes" => "",
   "status" => "",
   "amount" => "",
            ];
    }
}
