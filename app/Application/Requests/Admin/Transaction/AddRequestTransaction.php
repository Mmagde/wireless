<?php
 namespace App\Application\Requests\Admin\Transaction;
 use Illuminate\Foundation\Http\FormRequest;
 class AddRequestTransaction extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
     /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        	"transactiontype_id" => "required|integer",
            "from_id" => "required|integer",
            "to_id" => "required|integer",
            "notes" => "",
   "status" => "",
   "amount" => "",
            ];
    }
}
