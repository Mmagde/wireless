<?php

namespace App\Application\Transformers;

use Illuminate\Database\Eloquent\Model;

class AccountTransformers extends AbstractTransformer
{

    public function transformModel(Model $modelOrCollection)
    {
        return [
            "id" => $modelOrCollection->id,
			"name" => $modelOrCollection->name,
			"balance" => $modelOrCollection->balance,

        ];
    }

    public function transformModelAr(Model $modelOrCollection)
    {
        return [
           "id" => $modelOrCollection->id,
			"name" => $modelOrCollection->name,
			"balance" => $modelOrCollection->balance,

        ];
    }

}