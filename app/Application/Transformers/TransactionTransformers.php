<?php

namespace App\Application\Transformers;

use Illuminate\Database\Eloquent\Model;

class TransactionTransformers extends AbstractTransformer
{

    public function transformModel(Model $modelOrCollection)
    {
        return [
            "id" => $modelOrCollection->id,
            "from_id" => $modelOrCollection->from_id,
            "to_id" => $modelOrCollection->to_id,
            "transactiontype_id" => $modelOrCollection->transactiontype_id,
			"notes" => $modelOrCollection->notes,
			"status" => $modelOrCollection->status,
			"amount" => $modelOrCollection->amount,
            "date" => $modelOrCollection->created_at
        ];
    }

    public function transformModelAr(Model $modelOrCollection)
    {
        return [
           "id" => $modelOrCollection->id,
			"notes" => $modelOrCollection->notes,
			"status" => $modelOrCollection->status,
			"amount" => $modelOrCollection->amount,

        ];
    }

}