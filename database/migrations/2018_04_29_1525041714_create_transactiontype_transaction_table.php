<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactiontypetransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    		if (!Schema::hasColumn("transaction", "transactiontype_id"))
		{
	Schema::table("transaction", function (Blueprint $table)  {
		$table->integer("transactiontype_id")->unsigned();
		$table->foreign("transactiontype_id")->references("id")->on("transactiontype")->onDelete("cascade");

	});		}

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::disableForeignKeyConstraints();
		if (Schema::hasColumn("transaction", "transactiontype_id"))
		{
			$arrayOfKeys = $this->listTableForeignKeys("transaction");
			Schema::table("transaction", function ($table) use ($arrayOfKeys) {
			Schema::disableForeignKeyConstraints();
				if(in_array("transaction_transactiontype_id_foreign" , $arrayOfKeys)){
					$table->dropForeign("transaction_transactiontype_id_foreign");
					$table->dropColumn("transactiontype_id");
				}else{
					$table->dropColumn("transactiontype_id");
				}
			Schema::enableForeignKeyConstraints();
			});
		}
		Schema::enableForeignKeyConstraints();

    }
}
