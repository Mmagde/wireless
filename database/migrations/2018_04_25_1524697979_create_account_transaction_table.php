<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccounttransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    		if (!Schema::hasColumn("transaction", "account_id"))
		{
	Schema::table("transaction", function (Blueprint $table)  {
		$table->integer("account_id")->unsigned();
		$table->foreign("account_id")->references("id")->on("account")->onDelete("cascade");

	});		}

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::disableForeignKeyConstraints();
		if (Schema::hasColumn("transaction", "account_id"))
		{
			$arrayOfKeys = $this->listTableForeignKeys("transaction");
			Schema::table("transaction", function ($table) use ($arrayOfKeys) {
			Schema::disableForeignKeyConstraints();
				if(in_array("transaction_account_id_foreign" , $arrayOfKeys)){
					$table->dropForeign("transaction_account_id_foreign");
					$table->dropColumn("account_id");
				}else{
					$table->dropColumn("account_id");
				}
			Schema::enableForeignKeyConstraints();
			});
		}
		Schema::enableForeignKeyConstraints();

    }
}
